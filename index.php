<?php
require "vendor/autoload.php";
$faker = Faker\Factory::create('fr_FR');


// Routing
$page = 'home';


// ne sais pas exactment à quoi servent les 3 lignes du dessous, ça marche très bien sans o:
// if (isset($_GET['p'])) {
// $page = $_GET['p'];
// }

// rendu TWIG Template

$loader = new Twig_Loader_Filesystem(__DIR__ . '/template');
$twig = new Twig_Environment($loader, [

'cache' =>false //__DIR__ . '/temp'

]);

if ( $page === 'home') {

    echo $twig->render('fakerindex.twig', [ 'faker' =>[
    'companyname' => $faker->company,
    'phrase' => $faker->catchPhrase,
    'produitAdj' => $faker->word,
    'produitname' => $faker->word,
    'produitmatos'=> $faker->word,
    'url'=> $faker->url,
    'squareimage'=> $faker->imageUrl($width = 640, $height = 480),
    'color'=> $faker->hexcolor,//  (safeColorName)
    'price' => $faker->numberBetween($min = 10, $max = 900),
    'roundimage'=> $faker->imageUrl($width = 150, $height = 150),
    'username1' => $faker->firstName,
    'username2' => $faker->lastName,
    'job' => $faker->jobTitle,
    'email' => $faker->safeEmail,
    'phone' => $faker->mobileNumber,
    'numrue' => $faker->numberBetween($min = 1, $max = 100),
    'rue'=> $faker->streetName,
    'codepostal'=> $faker->postcode,
    'ville'=> $faker->city,
    ]
]);

}





?>